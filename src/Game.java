import java.util.*;
public class Game {
	Scanner inp = new Scanner(System.in);
	Board b =new Board();
	Player player = new Player();
	//set variable 
	private int inpR, inpC;
	
	// cons
	public Game() {
		
	}
	// function
	public void input() {
		inpR = inp.nextInt();
		inpC = inp.nextInt();
	}
	
	public void printWelcome() {
		System.out.println("Welcome to OX Game");
	}

	public void printTable() {
		for (int i = 0; i < b.getTable().length; i++) {
			for (int k = 0; k < b.getTable().length; k++) {
				System.out.print(b.getTable()[i][k]+" ");
			}
			System.out.println();
		}
	}

	public void printTurn() {
		System.out.println("TURN " + player.getName());
		System.out.println("KEY ( ROW, COL ) FOR SELECT POSITION : ");
	}

}